# Calculator
## Very advanced calculator in Python and C++.
### Keeps order of operation.

Usage `./calc.py "( 3 ** 4 * ( 5 / // 6 ) ) + 7 - 8"` or `./calc "( 3 ** 4 * ( 5 / // 6 ) ) + 7 - 8"`, returns `164.34055763786452`
